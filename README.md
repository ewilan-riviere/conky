# Install Conky

With Debian like distrib, go to root of this repository

```bash
sudo apt install -y conky-all lm-sensors
cp .conkyrc ~/
cp Conky.desktop ~/.config/autostart/
```

Launch conky

```bash
conky -b
```

## Configuration links

- [linuxtricks.fr/conky-des-variables-systeme-sur-le-bureau](https://www.linuxtricks.fr/wiki/conky-des-variables-systeme-sur-le-bureau)
- [manjaro.org/Basic_Tips_for_conky](https://wiki.manjaro.org/index.php?title=Basic_Tips_for_conky)
- [conky.sourceforge.net](http://conky.sourceforge.net/variables.html)
- [linux.com/experts-guide-configuring-conky](https://www.linux.com/news/experts-guide-configuring-conky/)
- [infotrux.free.fr/ubuntu-conky-simple](http://infotrux.free.fr/index.php/ubuntu-conky-simple/)
- [linuxpedia.fr/personaliser/conky](https://linuxpedia.fr/doku.php/personaliser/conky)

Sensors
[doc.ubuntu-fr.org/lm-sensors](https://doc.ubuntu-fr.org/lm-sensors)